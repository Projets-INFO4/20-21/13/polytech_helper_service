import os
import re
import openpyxl, csv, json
from typing import Dict
from parse import clean_activity_name

inpathname = '../data/courses_baseline_data/'
inpathname2 = '../data/'

def parse_activity_code_toJSON(inpath) :

    """Parse cvs baselines files and creates JSON file that contains each activity code and activity name."""

    dico={}

    for file in os.listdir(inpath):

        with open( inpath+file, newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='"')
            for row in reader:
                #Variable "code" is the APOGEE code 
                code = row[0]
                if code and code.__len__() == 8:
                        print('Parsing : '+code+'...')
                        dico[code]=clean_activity_name(row[1])
        csvfile.close()
            
    with open('../data/activities_keys.json', 'w') as fp:
        json.dump(dico, fp, ensure_ascii=False, indent=4)
    fp.close()
    print('File compiled : /data/activities_keys.json')


def convert_duration_StrToFloat(s  : str):
    """Takes a String and check if it can be converted in float. Returns a float if possible or returns the given string otherwise."""  
    if not s:
        return 0.0
    elif s.isdigit() : 
        return float(s)
    else :
        tmp = s.replace('.','',1)
        if tmp.isdigit() :
            return float(s)
        else :
            return clean_activity_name(s)


def init_ind_column_actvities(row ) -> Dict:
    """Initiates the right column indexes to extract hours in the baseline tables"""

    ind_activities = {'NAME':1, 'CM':6, 'TD':8, 'TP':10, 'EXAM':16} #default

    for i in range(1,len(row)):
        row[i] = clean_activity_name(row[i])

        if (row[i]=="COURS" or row[i]=="CM"):
            ind_activities['CM']=i
        elif row[i]=="TD":
            ind_activities['TD']=i
        elif row[i]=="TP":
            ind_activities['TP']=i
        elif row[i]=="EXAM":
            ind_activities['EXAM']=i

    return ind_activities
            

def parse_baseline_toJSON(inpath,filename,outpath) :

    """Parse the cvs baseline file 'filename' and create a JSON file that contains each activity APOGEE code, name, and hours levels."""
    
    dico={}
    ind_activities = {'NAME':1, 'CM':6, 'TD':8, 'TP':10, 'EXAM':16} #default values

    firstline = True
    with open( inpath+filename, newline='') as csvfile:
        filename = re.sub('.csv$','',filename)
        print('Generating : '+filename+'.json'+' ...')
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        semester = 0
        semesterName = 'S'
        for row in reader:
            #Variable "code" is the APOGEE code 
            code = row[0]
            if code and code.__len__() == 8:
                    if firstline :
                        ind_activities = init_ind_column_actvities(row)
                        #print(ind_activities)
                        firstline = False
                    #print('Parsing : '+code+'...')
                    if(re.search("^KA...S01$",code)):
                        semester += 1
                        semesterName = 'S'+str(semester)
                        dico[semesterName]={}
                        continue
                    activity = {}
                    for i in ind_activities:
                        activity[i] = convert_duration_StrToFloat(row[ind_activities[i]])
                    dico[semesterName][code]=activity
    csvfile.close()

    with open(outpath+filename+'.json', 'w') as fp:
        json.dump(dico, fp, ensure_ascii=False, indent=4)
    fp.close()

def all_baselines_toJSON(inpath,outpath) :
    """Remove all files in the outpath directory. Then parse csv baseline files into json files."""
    for f in os.listdir(outpath):
            os.remove(outpath+f)
    for f in os.listdir(inpath):
        if(not re.search(".csv$",f)):
            continue
        parse_baseline_toJSON(inpath,f,outpath)

def export_baseline_toCSV(inpath,filename,outpath):

    """Parse xlsx baseline 'filename' into cvs file."""

    wb = openpyxl.load_workbook(inpath + filename)
    sh = wb.active
    filename = re.sub('.xlsx$','',filename)
    dst_file = filename + '.csv'
    with open(outpath + dst_file , 'w', newline="") as f:
        print('Generating : '+dst_file+' ...')
        col = csv.writer(f)
        for row in sh.rows:
            col.writerow([cell.value for cell in row])

def all_baseline_toCSV(inpath,outpath):

    """Remove all files in the outpath directory. Then parse xlsx baseline files into cvs files."""

    for f in os.listdir(outpath):
        os.remove(outpath+f)
    for src_file in os.listdir(inpath):
        if(not re.search(".xlsx$",src_file)):
            continue
        export_baseline_toCSV(inpath,src_file,outpath)

def generate_all_baselines():
    """generate all the baselines from ../data/courses_baseline_xlsx/ (.xlsx) to ../data/courses_baseline_data/ (.json)"""
    all_baseline_toCSV("../data/courses_baseline_xlsx/","../data/courses_baseline_csv/")
    all_baselines_toJSON("../data/courses_baseline_csv/","../data/courses_baseline_data/")

def generate_baseline(filename):
    """generate the baseline corresponding to 'filename' from .xlsx to .json"""
    export_baseline_toCSV("../data/courses_baseline_xlsx/", filename, "../data/courses_baseline_csv/")
    filename = re.sub('.xlsx','.csv',filename)
    parse_baseline_toJSON("../data/courses_baseline_csv/", filename, "../data/courses_baseline_data/")
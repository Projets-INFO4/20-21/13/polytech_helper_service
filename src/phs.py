import os
import click
import re
import json
import datetime
from generatebaseline import generate_baseline, generate_all_baselines
from parse import build_groups_list, get_cal, parse_cal, compute_time_activity, compute_time_teachers, error_reporting, activityHV_to_str, teacherHV_to_str, errors_to_str, compare_hourly_volumes, comparison_to_str

fp = open("../data/agenda_ids_2019_2020.json", "r")
ids = json.load(fp)
fp.close

def second_layer():
    """Make a list of all second layer groups from ids (i.e. INFO 3, INFO 4, INFO 5...)"""
    groups=[]
    for k in ids:
        if isinstance(ids[k],dict):
            for k2 in ids[k]:
                groups.append(k2)
        else:
            groups.append(k)
    return groups

def get_id(group):
    """Return a list of all the ids contained in the group 'group'"""
    sub = sub_get_id(group,ids)
    res = []
    res = build_id_list(sub, res)
    return res

def sub_get_id(group,dico):
    """Recursive function to get the value of key 'group'."""
    if(isinstance(dico,dict)):
        if(group in dico.keys()):
            sub = dico[group]
            return sub
        else:
            for k in dico:
                sub = sub_get_id(group,dico[k])
                if sub:
                    return sub
    return None

def build_id_list(data, res):
    """Retrun a list composed of all values present in res"""
    if(isinstance(data,dict)):
        for k in data:
            res = build_id_list(data[k], res)
    else:
        res.append(data)
    return res

@click.group()
def cli():
    pass

@cli.command(help='Display a report of comparison between activities hourly volumes of a baseline and those calculated from ADE information.')
@click.option('--group', prompt='Please select the group', type=click.Choice(second_layer(), case_sensitive=False), help='Group to consider for information comparison.')
@click.option('--period', prompt='Please select the time period for the comparison',type=click.Choice(['Year','Semester'], case_sensitive=False), help='Period to consider.')
@click.option('--baseline', prompt='Please enter the name of the baseline file (.xlsx) to use', help='Baseline file to use (must be a .xlsx file, in data/courses_baseline_xlsx/)')
@click.option('--refresh', is_flag=True, help='Flag indicating to replace (=generate again) the data from the indicated baseline file.')
@click.option('--errors', is_flag=True, help='Flag indicating to display error reporting (error detected for each activity present on ADE for group and period).')
def compare(period, group, baseline, refresh, errors):
    # ask a valid filename
    while(not os.path.isfile("../data/courses_baseline_xlsx/"+baseline)):
        click.echo("Error : File "+baseline+" doesn't exist !")
        baseline = click.prompt('Please enter the name of the baseline file (.xlsx) to use')
    jsonName = re.sub(".xlsx$",".json",baseline)
    if period == 'Year':
        start = datetime.datetime.fromisoformat('2020-09-01')
        end = datetime.datetime.fromisoformat('2021-07-31')
        semester = 'all'
    else:
        while(True):
            start = click.prompt('Please enter the start date (format YYYY-MM-DD)', type=click.DateTime(['%Y-%m-%d']), default=datetime.datetime.fromisoformat('2020-09-01'))
            end = click.prompt('Please enter the end date (format YYYY-MM-DD)', type=click.DateTime(['%Y-%m-%d']), default=datetime.datetime.fromisoformat('2021-01-10'))
            if(start<end):
                break
            else:
                click.echo('Error : start date must be earlier than end date !')
        semester = click.prompt('Please enter the semester to consider (first or second)',type=click.Choice(['first','second']), default = 'first')
    #refresh or json does not exist : regenerate baseline
    if(refresh or not os.path.isfile("../data/courses_baseline_data/"+jsonName)):
        generate_baseline(baseline)
    #get list of ids corresponding to the group
    ids_group = get_id(group)
    list_ids = ",".join(ids_group)
    #get ADE information
    ade = get_cal(list_ids,start.date().isoformat(),end.date().isoformat())
    adeParsed = parse_cal(ade)
    #compare
    comparison,missing = compare_hourly_volumes("../data/courses_baseline_data/"+jsonName,adeParsed,semester)
    click.echo(comparison_to_str(comparison,missing))
    if(errors):
        click.echo(errors_to_str(error_reporting(adeParsed)))

@cli.command(help='Display a report of the activities or teachers hourly volumes, extracted from ADE, for a group and a period.')
@click.option('--group', prompt='Please select the group', type=click.Choice(build_groups_list(), case_sensitive=False), show_choices=False, help='Group to consider for information extraction.')
@click.option('--start', prompt='Please enter the start date (format YYYY-MM-DD)', type=click.DateTime(['%Y-%m-%d']), default='2020-09-01', help='Get ADE information from this date.')
@click.option('--end', prompt='Please enter the end date (format YYYY-MM-DD)', type=click.DateTime(['%Y-%m-%d']), default='2021-07-31', help='Get ADE information until this date.')
@click.option('--mode', type=click.Choice(['activities','teachers','both'], case_sensitive=False), default='activities', help='Indicate the mode of hourly volume (HV) computing and display : activities for HV by activity; teachers for HV by teacher; both for activities and teachers HV.')
@click.option('--errors', is_flag=True, help='Flag indicating to display error reporting (error detected for each activity present on ADE for group and period).')
def display(group, start, end, mode, errors):
    while(start>end):
        click.echo('Error : start date must be earlier than end date !')
        start = click.prompt('Please enter the start date (format YYYY-MM-DD)', type=click.DateTime(['%Y-%m-%d']), default=datetime.datetime.fromisoformat('2020-09-01'))
        end = click.prompt('Please enter the end date (format YYYY-MM-DD)', type=click.DateTime(['%Y-%m-%d']), default=datetime.datetime.fromisoformat('2021-07-31'))
    #get list of ids corresponding to the group
    ids_group = get_id(group)
    list_ids = ",".join(ids_group)
    #get ADE information
    ade = get_cal(list_ids,start.date().isoformat(),end.date().isoformat())
    adeParsed = parse_cal(ade)
    #calculate and display hourly volumes
    if(mode == 'activities' or mode == 'both'):
        activity_sorted = compute_time_activity(adeParsed)
        click.echo(activityHV_to_str(activity_sorted))
    if(mode == 'teachers' or mode == 'both'):
        teacher_sorted = compute_time_teachers(adeParsed)
        click.echo(teacherHV_to_str(teacher_sorted))
    if(errors):
        click.echo(errors_to_str(error_reporting(adeParsed)))

@cli.command(help='Generate all useful data from the data/courses_baseline_xlsx/ directory. Delete old generated data.')
def generate():
    generate_all_baselines()

if __name__ == '__main__':
    cli()

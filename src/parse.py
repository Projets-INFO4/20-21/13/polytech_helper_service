from datetime import datetime
from datetime import timedelta
import os
import requests
import ics, csv, json
import re
import unidecode

def get_cal(cal_id: str,startDate, endDate) -> str:
    """Download the ADE agenda according to the given id, start and end date"""
    url = "http://ade-sts.grenet.fr/jsp/custom/modules/plannings/anonymous_cal.jsp?resources="+cal_id+"&projectId=2&calType=ical&firstDate="+startDate+"&lastDate="+endDate
    response = requests.get(url)
    if response.headers['content-type'] != "text/calendar;charset=UTF-8":
        raise TypeError("HTTP Response not a Calendar : " +
                        response.headers["content-type"])
    return response.text

def parse_cal(data: str) :
    """Parse a given ics formated string into an ordered dictionary"""
    options_list = build_groups_list()
    regex = load_regex_file()
    adeParsed = {}
    for e in list(ics.Calendar(data).events):
        adeParsed[e.uid] = {}
        adeParsed[e.uid]["error"] = [] #field for error reporting
        adeParsed[e.uid]["description"] = e.description.split("(Exp")[0]
        adeParsed[e.uid]["location"] = e.location
        adeParsed[e.uid]["summary"] = clean_activity_name(e.name)
        adeParsed[e.uid]["start"] = e.begin.datetime
        adeParsed[e.uid]["end"] = e.end.datetime
        adeParsed[e.uid]["duration"] = e.end.datetime - e.begin.datetime
        adeParsed[e.uid] = parse_description(adeParsed[e.uid],options_list, regex) #add groups and teachers fields, keys : 'groups' and 'teachers'
        adeParsed[e.uid] = parse_activity_type(adeParsed[e.uid], regex) #add activity type field, key : 'activityType'
        adeParsed[e.uid] = fill_errors(adeParsed[e.uid], regex)
    return adeParsed

def load_regex_file():
    """Load the regex file data/regex.json"""
    fp = open("../data/regex.json", "r")
    regex = json.load(fp)
    fp.close()
    return regex

def load_activityCode() :
    """Load a JSON file and return a dictionary that contains each activity APOGEE code and name."""

    fp = open('../data/activities_keys.json', newline='')
    codes = json.load(fp)
    fp.close()

    return codes 

def build_groups_list():
    """Returns a list of the students groups."""
    fp = open("../data/agenda_ids_2019_2020.json", "r")
    ids = json.load(fp)
    fp.close()
    options = []
    for i in ids.keys():
        options.append(i)

    for e in ids:
        for i in ids[e].keys():
            options.append(i)
        for c in ids[e]:
            for i in ids[e][c].keys():
                options.append(i)
    return options


def filtered_list(lst, item):
    """Removes from "lst" all occurences of "item" """
    return [i for i in lst if i != item]

def parse_description(event, options, regex):
    """Parse groups and teachers from an event description and returns the completed event."""
    description = event['description']
    lines = description.split('\n')
    lines = filtered_list(lines,'') #remove blank lines
    groups = []
    teachers = []
    for l in lines:
        if l in options:
            groups.append(l) #add groups
        else:
            l = unidecode.unidecode(l) #remove accents
            l = l.upper() #put string in upper case
            l = l.strip(" ") #remove useless blanks at beginning and end of string
            reg = re.compile(regex["teachers"],flags=re.I)
            if reg.search(l):
                teachers.append(l) #add teachers
            reg2 = re.compile(regex["missing_info"])
            if reg2.search(l):
                if "MISSING_TEACHER" not in event['error']:
                    event['error'].append("MISSING_TEACHER") #add error
    event['groups'] = groups
    event['teachers'] = teachers
    return event

def parse_activity_type(event, regex):
    """Parse activity type from an agenda event summary and returns a dictionary."""
    summary = event['summary']
    event['activityType'] = "CM" #default value
    for r in ["TD","TP","CM","EXAM","Other"]:
        reg = re.compile(regex[r],flags=re.I)
        res = reg.search(summary)
        if res!=None:
            event['activityType'] = r
            name = re.sub(regex[r]," ",summary,flags=re.I) #remove activity type from summary
            name = name.strip(" ")
            event['summary'] = name
            break;
    return event

def clean_activity_name(name):
    """Clean an activity name by removing accents, acronyms, dashes, useless blanks. Put it in upper case."""
    name = unidecode.unidecode(name) #remove accents
    name = name.upper() #put string in upper case
    name = re.sub('\([A-Z ]*\)',"",name) #remove activity acronym
    name = re.sub('-'," ",name) #replace dashes by spaces
    name = re.sub(' ( )+'," ",name) #replace multiple spaces by only one space
    name = name.strip(" ") #remove useless blanks at beginning and end of string
    return name

def fill_errors(event, regex):
    """Fill error field of an agenda event if needed."""
    reg = re.compile(regex["missing_info"])
    if (not event["location"]) | (reg.search(event["location"])!=None):
        event['error'].append("MISSING_LOCATION")
    if (not event["summary"]) | (reg.search(event["summary"])!=None):
        event['error'].append("MISSING_SUMMARY")
    if not event["start"]:
        event['error'].append("MISSING_START_DATETIME")
    if not event["end"]:
        event['error'].append("MISSING_END_DATETIME")
    if (not event["teachers"]) & ("MISSING_TEACHER" not in event['error']):
        event['error'].append("MISSING_TEACHER")
    return event

def compute_time_activity(adeParsed):
    """Compute, for each of activity, the hourly volume of each activity type. Return a dictionary."""
    result = {}
    for e in adeParsed:
        event = adeParsed[e]
        summary = event['summary']
        if summary not in result:
            result[summary] = {}
        actType = event['activityType']
        if actType not in result[summary]:
            result[summary][actType] = 0.0
        result[summary][actType] += event['duration'].seconds/3600
    return result

def compute_time_teachers(adeParsed):
    """Compute, for each teacher and for each of his activity, his hourly volume depending on activity type. Return a dictionary."""
    result = {}
    for e in adeParsed:
        event = adeParsed[e]
        teachers = event['teachers']
        for t in teachers:
            if t not in result:
                result[t] = {}
            summary = event['summary']
            if summary not in result[t]:
                result[t][summary] = {}
            actType = event['activityType']
            if actType not in result[t][summary]:
                result[t][summary][actType] = 0.0
            result[t][summary][actType] += event['duration'].seconds/3600
    return result

def error_reporting(adeParsed):
    """Return a dictionary containing all the events for which errors have been detected."""
    errorReporting = []
    for e in adeParsed:
        event = adeParsed[e]
        if(event['error']):
            errorReporting.append(event)
    return errorReporting

def activityHV_to_str(ahv):
    """Build a string representation to display activity hourly volume."""
    rep = "\n#=========================\n# ACTIVITIES HOURLY VOLUME\n#=========================\n\n"
    for activity in ahv:
        rep += activity+":\n"
        for t in ahv[activity]:
            rep += "\t"+str(t)+": "+str(ahv[activity][t])+"\n"
        rep += "\n"
    return rep

def teacherHV_to_str(thv):
    """Build a string representation to display teache hourly volume."""
    rep = "\n#=========================\n# TEACHERS HOURLY VOLUME\n#=========================\n\n"
    for teacher in thv:
        rep += teacher+":\n"
        for activity in thv[teacher]:
            rep += "\t"+activity+":\n"
            for t in thv[teacher][activity]:
                rep += "\t\t"+str(t)+": "+str(thv[teacher][activity][t])+"\n"
        rep += "\n"
    return rep

def errors_to_str(er):
    """Build a string representation to display error reporting."""
    rep = "\n#=========================\n# ERROR REPORTING\n#=========================\n\n"
    if not er:
        rep += "No error detected"
        return rep
    for event in er:
        sDate = event['start'].date()
        sTime = event['start'].time().replace(hour = event['start'].time().hour+1) #update time to the correct hour
        eDate = event['end'].date()
        endTime = event['end'].time().replace(hour = event['end'].time().hour+1) #update time to the correct hour
        rep += event['summary']+" from "+sDate.isoformat()+" at "+sTime.isoformat()+" to "+eDate.isoformat()+" at "+endTime.isoformat()+" :\n"
        rep += "Error(s) detected: "+", ".join(event['error'])+"\n\n"
    return rep

def compare_hourly_volumes(baselinePath,adeParsed,semester):
    """Compare the activity hourly volumes between a baseline and an export ADE.
        param :
            baseline : path to JSON file representing the baseline data to compare
            adeParsed : export of ADE parsed in order to compare it
            semester : indicates the semester to consider, values : 0 (complete year), 1 (1st semester) or 2 (2nd semester)"""
    fp = open(baselinePath, 'r')
    baseline = json.load(fp) #load data from baseline
    fp.close()
    ahv = compute_time_activity(adeParsed)
    s = []
    if(semester == 'all'):
        s = ['S1','S2']
    elif(semester == 'first'):
        s = ['S1']
    elif(semester == 'second'):
        s = ['S2']
    comparison = {}
    for k in s:
        base = baseline[k]
        for activity in base:
            if(not re.search('^KA...[UOTS]0.$|KA..U0..$',activity)): # U = UE, O = Option, T = Stage, S = Semestre; U = UE TC
                comparison[activity] = {}
                comparison[activity]['name'] = base[activity]['NAME']
                comparison[activity]['error'] = []
                hours = {}
                #search corresponding activity in activity hourly volume dictionary:
                for e in ahv:
                    if(e and e in base[activity]['NAME']):
                        hours = ahv[e]
                        ahv[e]['compFlag'] = True #indicate this activity correspond to one from the baseline
                        break
                if not hours: #activity not found
                    comparison[activity]['error'].append("This activity was not found in ADE")
                for elem in ['CM','TD','TP','EXAM']:
                    computed = 0.0
                    expected = base[activity][elem]
                    try:
                        computed = hours[elem]
                    except KeyError:
                        pass
                    comparison[activity][elem] = {'expected':expected,'computed':computed}
                    if(expected != computed):
                        comparison[activity]['error'].append(elem +": incoherent hourly volume")
    #Get all activities not matched with baseline
    missing = {}
    for elem in ahv:
        if('compFlag' not in ahv[elem].keys()):
            missing[elem] = ahv[elem]
    return (comparison,missing)

def comparison_to_str(comparison, missing):
    """Build a string representation to display comparison result."""
    rep = "\n#=========================\n# COMPARISON\n#=========================\n\n"
    for activity in comparison:
        elem = comparison[activity]
        rep += activity+" "+elem['name']+":\n"
        if(elem['error']):
            rep += "\t"+"Error(s) detected: "+", ".join(elem['error'])+"\n"
        for t in ['CM','TD','TP','EXAM']:
            rep += "\t"+t+": expected="+str(elem[t]['expected'])+"; computed="+str(elem[t]['computed'])+"\n"
        rep += "\n"
    #representation for activities not matched
    if(missing):
        rep += "\n================== Activities computed from ADE but not matched with baseline ==================\n"
    for act in missing:
        rep += act+":\n"
        for k in missing[act]:
            rep += "\t"+k+": "+str(missing[act][k])+"\n"
        rep += "\n"
    return rep
